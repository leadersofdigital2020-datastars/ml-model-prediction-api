FROM debian:latest

#  $ docker build . -t continuumio/miniconda3:latest -t continuumio/miniconda3:4.5.11
#  $ docker run --rm -it continuumio/miniconda3:latest /bin/bash
#  $ docker push continuumio/miniconda3:latest
#  $ docker push continuumio/miniconda3:4.5.11

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN apt-get update --fix-missing && \
    apt-get install -y wget bzip2 ca-certificates curl git python3-pip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-4.5.11-Linux-x86_64.sh -O ~/miniconda.sh && \
#     /bin/bash ~/miniconda.sh -b -p /opt/conda && \
#     rm ~/miniconda.sh && \
#     /opt/conda/bin/conda clean -tipsy && \
#     ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
#     echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
#     echo "conda activate base" >> ~/.bashrc && \
#     echo "conda activate model" >> ~/.bashrc
# 
# ENV TINI_VERSION v0.16.1
# ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
# RUN chmod +x /usr/bin/tini
# 
# RUN source create -n model python=3.7.9 anaconda
# RUN source activate model
# RUN source install flask numpy scikit-learn

RUN pip3 install numpy 
RUN pip3 install pandas 
RUN pip3 install flask
RUN pip3 install scikit-learn

WORKDIR /opt
RUN mkdir mlmodel
COPY main.py /opt/mlmodel/main.py
COPY model.pkl /opt/mlmodel/model.pkl

# ENTRYPOINT [ "/usr/bin/bash", "--" ]
WORKDIR /opt/mlmodel
# CMD [ "/bin/bash" ]
EXPOSE 5000
CMD [ "python3", "main.py" ]
