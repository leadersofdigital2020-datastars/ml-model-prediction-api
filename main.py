#!flask/bin/python
import pickle
import datetime
from sklearn.linear_model import LinearRegression
import numpy as np
from flask import Flask, jsonify, abort
from flask import request
from flask import make_response

app = Flask(__name__)

model = None


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/predict_1', methods=['POST'])
def predict_model1():
    if not request.json and "USE_FACT" in request.json:
        abort(400)
    print(request.json)

    month = int(request.json["DATE"][5:7])
    day = int(request.json["DATE"][8:9])
    weekday = datetime.datetime(int(request.json["DATE"][:4]), month, day).weekday()
    use_fact = float(request.json["USE_FACT"])
    temp = float(request.json["TEMP"])
    visib = float(request.json["VISIB"])
    use_fact1 = float(request.json["USE_FACT1"])
    use_fact3 = float(request.json["USE_FACT3"])
    use_fact5 = float(request.json["USE_FACT5"])
    use_fact7 = float(request.json["USE_FACT7"])
    use_fact14 = float(request.json["USE_FACT14"])
    use_fact30 = float(request.json["USE_FACT30"])
    temp1 = float(request.json["TEMP1"])
    temp3 = float(request.json["TEMP3"])
    temp5 = float(request.json["TEMP5"])
    temp7 = float(request.json["TEMP7"])
    temp14 = float(request.json["TEMP14"])
    temp30 = float(request.json["TEMP30"])
    ii = float(request.json["II"])
    ii3 = float(request.json["II3"])
    msp = float(request.json["MSP"])
    coms = float(request.json["COMS"])
    coms3 = float(request.json["COMS3"])

    np_data = np.array([[month, day, weekday, use_fact, temp, visib,
                         use_fact1, use_fact3, use_fact5, use_fact7, use_fact14, use_fact30,
                         temp1, temp3, temp5, temp7, temp14, temp30,
                         ii, ii3, coms, coms3, msp
                         ]])
    result = model.predict(np_data)

    return jsonify({f"res{i}": x for i, x in enumerate(result[0])}), 201


@app.route('/')
def index():
    return "Energy predict"


if __name__ == '__main__':
    with open('model.pkl', 'rb') as handle:
        model = pickle.load(handle)
    # app.run(debug=True, host='127.0.0.1', port=5000)
    app.run(debug=False, host='0.0.0.0', port=5000)
